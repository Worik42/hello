package com.example.inkubator.hello;

import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends MvpAppCompatActivity implements ViewMV {

    public static final String UrlConn = "http://10.15.14.119";
    @InjectPresenter
    PresMV mPresMV;
    @BindView(R.id.texHell)
    TextView mHelloWorldTextView;
    @BindView(R.id.texHello)
    TextView mHelloWorld;
    ApiService apiService;
    String str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(UrlConn)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiService.class);
        Call<HelloN> call = apiService.getMyJSON();
        call.enqueue(new Callback<HelloN>() {
            @Override
            public void onResponse(Call<HelloN> call, Response<HelloN> response) {
                HelloN hellon = response.body();
                str = hellon.getMessage();
                mHelloWorld.setText(str);
            }

            @Override
            public void onFailure(Call<HelloN> call, Throwable t) {

            }
        });


    }


    @Override
    public void showMessage(int message) {
        mHelloWorldTextView.setText(message);
    }

}
