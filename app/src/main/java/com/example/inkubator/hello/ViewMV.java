package com.example.inkubator.hello;

import com.arellomobile.mvp.MvpView;

public interface ViewMV extends MvpView {
    void showMessage(int message);
}