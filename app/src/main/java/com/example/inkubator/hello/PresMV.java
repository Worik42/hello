package com.example.inkubator.hello;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

/**
 * Created by inkubator on 06.07.17.
 */
@InjectViewState
public class PresMV extends MvpPresenter<ViewMV> {
    public PresMV() {
        getViewState().showMessage(R.string.hello_world);
    }
}

