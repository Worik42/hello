package com.example.inkubator.hello;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by inkubator on 06.07.17.
 */

public interface ApiService {

    @GET("/hello")
    Call<HelloN> getMyJSON();
}
